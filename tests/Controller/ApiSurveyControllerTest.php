<?php

namespace App\Test\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use App\Entity\Survey;

class ApiSurveyControllerTest extends WebTestCase
{
    protected $client;

    protected $data = [];

    /**
    * @var EntityManagerInterface
    */
    protected $em;

    public function setUp()
    {
        $this->client = static::createClient();
        $this->data = [
            "title" => "question 1",
            "promoted" => 1,
            "status" => "draft",
            "answers" => [
                0 => [
                    "channel" => "faq",
                    "body" => "toto"
                ]
	        ]
        ];

        $this->em = self::$kernel->getContainer()->get('doctrine.orm.entity_manager');        
    }

    public function testPostSurveyReturn201()
    {
        $this->client->request(
            'POST', 
            '/api/survey', 
            [],
            [],
            [],
            json_encode($this->data)
        );

        $response = $this->client->getResponse();

        $this->assertEquals(Response::HTTP_CREATED, $response->getStatusCode());

        $survey = $this->em->getRepository(Survey::class)->findOneBy(['title' => 'question 1']);

        $this->assertEquals("question 1", $survey->getTitle());
    }


    public function testPostSurveyReturn400()
    {
        $this->data['answers'][0]['channel'] = 'toto';

        $this->client->request(
            'POST', 
            '/api/survey', 
            [],
            [],
            [],
            json_encode($this->data)
        );

        $response = $this->client->getResponse();

        $this->assertEquals(Response::HTTP_BAD_REQUEST, $response->getStatusCode());
    }

    public function testPostSurveyReturn400WhenErrorInJson()
    {
        $this->data = [
            "title2" => "question 1",
            "promoted" => 1,
            "status" => "draft",
            "answers" => [
                0 => [
                    "channel" => "faq",
                    "body" => "toto"
                ]
	        ]
        ];

        $this->client->request(
            'POST', 
            '/api/survey', 
            [],
            [],
            [],
            json_encode($this->data)
        );

        $response = $this->client->getResponse();

        $this->assertEquals(Response::HTTP_BAD_REQUEST, $response->getStatusCode());
    }

    public function testPutSurveyReturn202()
    {
        $survey = $this->em->getRepository(Survey::class)->findOneBy(['title' => 'question 1']);

        $this->data = [
            "title" => "question 2"
	    ];

        $this->client->request(
            'PUT', 
            '/api/survey/' . $survey->getId(), 
            [],
            [],
            [],
            json_encode($this->data)
        );

        $response = $this->client->getResponse();

        $this->assertEquals(Response::HTTP_ACCEPTED, $response->getStatusCode());
    }

    public function testPutSurveyReturn404()
    {
        $this->data = [
            "title" => "question 2"
	    ];

        $this->client->request(
            'PUT', 
            '/api/survey/453083', 
            [],
            [],
            [],
            json_encode($this->data)
        );

        $response = $this->client->getResponse();

        $this->assertEquals(Response::HTTP_NOT_FOUND, $response->getStatusCode());
    }
}

