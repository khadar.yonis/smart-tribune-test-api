<?php

namespace App\Command;

use App\Service\ExportCSV;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class ExportCSVCommand extends Command
{
    protected static $defaultName = 'app:export:historic';

    /**
     * @param ExportCSV $exportCSV
     */
    private $exportCSV;

    public function __construct(ExportCSV $exportCSV)
    {
        $this->exportCSV = $exportCSV;
        parent::__construct();

    }

    /**
     * Execute Command app:export:historic to export historic data into file CSV 
     * 
     * @param InputInterface $input
     * @param OutputInterface @output
     */
    public function execute(InputInterface $input, OutputInterface $output):int
    {
        $io = new SymfonyStyle($input, $output);

        if($this->exportCSV->exportCSV()) {
            $io->note('Le fichier historique (history.csv) a été bien exporté dans le dossier public.');
            return true;
        } else {
            $io->note('Erreur lors d\'enregistrement du fichier historique.');
        }       
    }
}
