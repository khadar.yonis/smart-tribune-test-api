<?php

namespace App\Service;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use App\Entity\Survey;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class UpdateSurvey
 * @package App\Service
 */
class UpdateSurvey
{
    /**
     * @var EntityManagerInterface
     */
    private $manager;

    /**
     * @var ValidatorInterface
     */
    private $validator;

    /**
     * UpdateSurvey constructor.
     * @param EntityManagerInterface $manager
     */
    public function __construct(EntityManagerInterface $manager, ValidatorInterface $validator)
    {
        $this->manager = $manager;
        $this->validator = $validator;
    }

    /**
     * Update Surveys
     * 
     * @param Request $request
     * @param Survey $survey
     * @return Survey
     * @throws \Exception
     */
    public function update(Request $request, Survey $survey)
    {
        $data = json_decode($request->getContent(), true);

        if (isset($data['title'])){
            $survey->setTitle($data['title']);
        }

        if (isset($data['status'])){
            $survey->setStatus($data['status']);
        }

        $survey->setUpdatedAt(new \DateTime());

        $errors = $this->validator->validate($survey);

        if (count($errors) > 0) {
            return $this->json([
                'status' => 400,
                'message' => $errors
            ], 400);
        }

        $this->manager->persist($survey);
        $this->manager->flush();

        return $survey;        
    }
}
