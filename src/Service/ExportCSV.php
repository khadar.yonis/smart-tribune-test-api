<?php

namespace App\Service;

use App\Repository\HistoricRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\StreamedResponse;

class ExportCSV
{
    private $historicRespository;

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * 
     * @param EntityManagerInterface $manager
     */
    public function __construct(EntityManagerInterface $em, HistoricRepository $historicRespository)
    {
        $this->em = $em;
        $this->historicRespository = $historicRespository;

    }

    /**
     * 
     * Export data historic to CSV
     */
    public function exportCSV()
    {
        try {
            $results = $this->historicRespository->findAll();
     
            $fp = fopen('./public/history.csv', 'w');
    
            foreach ($this->hydrate($results) as $fields) {
                
                fputcsv($fp, $fields);
            }        
            
            fclose($fp);

            return true;
        } catch (\Throwable $th) {
            dump($th);
        }

    }

    /**
     * Formatting result of findAll object($results) to Array
     * @param Array $results
     * @return Array
     */
    public function hydrate($results)
    {
        $output = $temp = [];

        foreach($results as $result) {
            $temp['message'] = $result->getMessage();
            $temp['survey_id'] = $result->getSurvey()->getId();
            $temp['date'] = date_format($result->getSurvey()->getCreatedAt() , 'Y-m-d H:i:s');

            array_push($output, $temp);
        }

        return $output;
    }
}
