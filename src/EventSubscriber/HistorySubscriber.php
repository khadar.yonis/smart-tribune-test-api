<?php

namespace App\EventSubscriber;

use Doctrine\ORM\Events;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Doctrine\Common\EventSubscriber;
use Doctrine\Persistence\Event\LifecycleEventArgs;
use App\Entity\Survey;
use App\Entity\Historic;
use App\Events\HistoryEvent;

class HistorySubscriber implements EventSubscriberInterface
{
    /**
     * @var EntityManagerInterface
     */
    private $manager;

    public function __construct(EntityManagerInterface $manager)
    {
        $this->manager = $manager;
    }

    public static function getSubscribedEvents()
    {
        return [
            HistoryEvent::NAME => 'saveHistory'
        ];
    }


    public function saveHistory(HistoryEvent $historyEvent)
    {
        $historic = new Historic();
        $historic->setSurvey($historyEvent->getSurvey());
        $historic->setMessage($historyEvent->getSurvey()->getTitle());
        $historic->setCreatedAt(new \DateTime());

        $this->manager->persist($historic); 
        $this->manager->flush();
    }

}