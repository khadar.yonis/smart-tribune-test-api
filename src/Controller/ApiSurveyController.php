<?php

namespace App\Controller;

use App\Entity\Survey;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Serializer\Exception\NotEncodableValueException;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\SurveyRepository;
use App\Repository\HistoricRepository;
use App\Service\UpdateSurvey;
use App\Service\ExportCSV;
use App\Events\HistoryEvent;

class ApiSurveyController extends AbstractController
{
    /**
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;

    /**
     * @var ExportCSV
     */
    private $exportCSV;

    /**
     * @var EntityManagerInterface
     */
    private  $manager;

    /**
     * @var SurveyRepository
     */
    private $surveyRepository;

    /**
     * @var
     */
    private $updateSurvey;

    /**
     * ApiSurveyController constructor.
     * 
     * @param EntityManagerInterface $manager
     * @param SurveyRepository $surveyRepository
     * @param UpdateSurvey $updateSurvey
     * @param ExportCSV $exportCSV
     * @param EventDispatcherInterface $eventDispatcher
     */
    public function __construct(EntityManagerInterface $manager, SurveyRepository $surveyRepository, UpdateSurvey $updateSurvey, ExportCSV $exportCSV, EventDispatcherInterface $eventDispatcher)
    {
        $this->manager = $manager;
        $this->surveyRepository = $surveyRepository;
        $this->updateSurvey = $updateSurvey;
        $this->exportCSV = $exportCSV;
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * Receive Q&A and store in a database (table survey)
     * 
     * @Route("/api/survey", name="api_survey_store", methods={"POST"})
     */
    public function store(Request $request, SerializerInterface $serializer, ValidatorInterface $validator) 
    {      
        $json = json_decode($request->getContent(), true);
        
        try {
            $survey = new Survey();

            $survey->setTitle($json['title']);
            $survey->setPromoted($json['promoted']);
            $survey->setStatus($json['status']);
            $survey->setChannel($json['answers'][0]['channel']);
            $survey->setBody($json['answers'][0]['body']);

            $survey->setCreatedAt(new \DateTime());
            $survey->setUpdatedAt(new \DateTime());

            $errors = $validator->validate($survey);
            
            if (count($errors) > 0) {
                return $this->json($errors, 400);
            }

            $this->manager->persist($survey);
            $this->manager->flush();

            return $this->json($survey, 201, []);
        } catch (\Exception $e) {
            return $this->json([
                'status' => 400,
                'message' => $e->getMessage()
            ], 400);
        }
    }

    /**
     * Update existing Q&A to change Title value as well as Status
     * 
     * @Route("/api/survey/{id}", name="api_survey_update", methods={"PUT"})
     */
    public function update(int $id, Request $request)
    {

        $survey = $this->surveyRepository->find($id);

        if (empty($survey)) {
            return $this->json([
                'status' => 404,
                'message' => 'Survey not found'
            ], 404);
        }

        $survey = $this->updateSurvey->update($request, $this->surveyRepository->find($id));        

        $this->eventDispatcher->dispatch(new HistoryEvent($survey), HistoryEvent::NAME);

        $response = new Response($this->get('serializer')->serialize($survey, 'json'), Response::HTTP_ACCEPTED);
    
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

    /**
     * Formatting JSON Request
     * 
     * @param Survey $survey
     */
    public function hydrate(Survey $survey)
    {
        return [
            'title'=> $survey->getTitle(),
            'promoted'=> $survey->getPromoted(),
            'status' => $survey->getStatus(),
            'answers' => [
                'channel' => $survey->getChannel(),
                'body' => $survey->getStatus()
            ]
        ];
    }
}
