<?php

namespace App\Events;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Contracts\EventDispatcher\Event;
use App\Entity\Survey;

/**
 * Class NextStepEvent
 * @package App\Events\Devis
 */
class HistoryEvent extends Event
{
    public const NAME = 'saveHistory';

    /**
     * @var Survey
     */
    private $survey;

    /**
     *
     * @param Survey $survey
     */
    public function __construct(Survey $survey)
    {
        $this->survey = $survey;
    }

    public function getSurvey(): ?Survey
    {
        return $this->survey;
    }

    
}