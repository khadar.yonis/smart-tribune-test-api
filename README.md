# API REST SYMFONY 5.1.1 & PostgresSql & Docker & Docker-compose

# Prérequis

- Docker & docker-compose
- Postman (pour tester l'API)


# Cloner le dépôt du projet
    `git clone https://gitlab.com/khadar.yonis/smart-tribune-test-api.git`

# Lancer le docker
    `docker-compose up --build -d`
    
# Lancer l'application

1. Vérifier la liste de conteneur avec la commande :
    `docker ps`

2. Lancer le conteneur du serveur "api-rest_smart-tribune_1":
    `docker exec -it api-rest_smart-tribune_1 bash`

3. Créer la base de données
    `php bin/console doctrine:database:create`

4. Lancer les migrations :
    `php bin/console doctrine:migrations:migrate`

Avec Postman :

> Step 1 Enregistrer une Question & Reponse (table survey)

1. Method : POST 
2. Endpoint : 0.0.0.0:81/api/survey avec body :
3. Body :
    {
    	"title": "question 1",
    	"promoted": false,
    	"status": "draft",
    	"answers": [{
    		"channel": "faq",
    		"body": "answer 1"
    	}]
    }
    

>  Step 2 Modifier une Question (table survey) et sauvegarder l'historique (table historic)

1. Method : PUT
2. Endpoint : 0.0.0.0:81/api/survey/1
3. Body :
    {
    	"title": "question 1 modified",
        "status": "published"
    }

>  Step 3 Exporter l'historique au format CSV en utilisant la ligne de commande Symfony à l'intérieur du conteneur
    `php bin/console app:export:historic`

# Lancement de Tests Unitaires (toujours à l'intérieur du conteneur)
   `bin\phpunit`

